<?php

/**
 * Install script to add new customer attribute - linkedin_profile
 */

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$entityTypeId     = $installer->getEntityTypeId('customer');

$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);

$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

if(!$installer->getAttributeId($entityTypeId, 'linkedin_profile')) {

    $installer->addAttribute("customer", "linkedin_profile", array(
        "type" => "varchar",
        "label" => "Linkedin Profile Link",
        "input" => "text",
        "visible" => true,
        "required" => false,
        "user_defined" => true,
        "unique" => false,
        "frontend_class" => "validate-length maximum-length-250 validate-url",
        "position" => 100
    ));
}

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "linkedin_profile");

$installer->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'linkedin_profile',
    '1000'
);

$used_in_forms=array(
    'adminhtml_customer',
    'customer_account_create',
    'customer_account_edit'
);

$attribute->setData("used_in_forms", $used_in_forms);
$attribute->save();

$installer->endSetup();