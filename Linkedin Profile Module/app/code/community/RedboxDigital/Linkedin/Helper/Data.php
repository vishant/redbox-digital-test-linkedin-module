<?php
class RedboxDigital_Linkedin_Helper_Data extends Mage_Core_Helper_Abstract
{
    const PATH_LINKEDIN_PROFILE_IS_REQUIRED = 'redboxdigital_linkedin/general/required';

    public function getLinkedinProfileFrontendClass(){
        /** @var $attribute Mage_Customer_Model_Attribute */
        $attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin_profile');
        $class = $attribute->getFrontend()->getClass();
        return $class;
    }

    public function getIsRequiredLinkedinProfile(){

        return Mage::getStoreConfig(self::PATH_LINKEDIN_PROFILE_IS_REQUIRED);
    }

    public function getLinkedinProfileLabelClass(){
        if ($this->getIsRequiredLinkedinProfile()){
            return 'required';
        } else return '';
    }

    public function getLinkedinProfileAsteriskSymbol(){
        if ($this->getIsRequiredLinkedinProfile()){
            return '<em>*</em>';
        } else return '';
    }

}