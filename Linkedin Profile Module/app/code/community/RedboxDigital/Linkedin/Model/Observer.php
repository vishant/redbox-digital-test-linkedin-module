<?php
class RedboxDigital_Linkedin_Model_Observer
{
    public function adminSystemConfigIsRequired(Varien_Event_Observer $observer)
    {
        $isRequired = Mage::getStoreConfig('redboxdigital_linkedin/general/required');

        $updateAttribute = Mage::getSingleton( 'eav/config' )
            ->getAttribute( 'customer', 'linkedin_profile' );

        if ($isRequired){
            $updateAttribute->setIsRequired(true);
        } else{
            $updateAttribute ->setIsRequired(false);
        }

        $updateAttribute->save();
    }
}